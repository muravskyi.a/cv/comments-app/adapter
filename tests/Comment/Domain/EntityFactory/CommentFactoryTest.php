<?php

namespace App\Tests\Comment\Domain\EntityFactory;

use App\Comment\Domain\Entity\Comment;
use App\Comment\Domain\ValueObject\Comment as ValueObjectComment;
use App\Tests\PrivatePropertyManipulatorTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class CommentFactoryTest extends KernelTestCase
{
  use PrivatePropertyManipulatorTrait;

  /** @test */
  public function itCreatesCommentEntityFromValueObject(): void
  {
    $commentId = '8ddaae1f-70ba-4de7-b16b-b0f300222258';
    $userId = '19264c50-1fdb-4648-aa90-15f9bacf1928';
    $topicId = 'abc78928-2419-4d0f-bce3-f75e74a727ca';
    $comment = 'test comment from comment factory test';

    $factory = $this->bootKernel()->getContainer()->get('Test.CommentFactory');

    $commentValObj = new ValueObjectComment();

    $this->setByReflection($commentValObj, 'commentId', $commentId);
    $this->setByReflection($commentValObj, 'userId', $userId);
    $this->setByReflection($commentValObj, 'topicId', $topicId);
    $this->setByReflection($commentValObj, 'comment', $comment);

    $commentEntity = $factory->createFromValueObject($commentValObj);

    $this->assertInstanceOf(Comment::class, $commentEntity);
    $this->assertEquals($commentId, $this->getByReflection($commentEntity, 'commentId'));
    $this->assertEquals($userId, $this->getByReflection($commentEntity, 'userId'));
    $this->assertEquals($topicId, $this->getByReflection($commentEntity, 'topicId'));
    $this->assertEquals($comment, $this->getByReflection($commentEntity, 'comment'));
  }
}
