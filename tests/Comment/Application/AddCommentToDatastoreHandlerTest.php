<?php

namespace App\Tests\Comment\Application;

use App\Comment\Application\AddCommentToDatastoreHandler;
use App\Comment\Domain\ValueObject\CommentUpsertedEvent;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Liip\TestFixturesBundle\Test\FixturesTrait;

final class AddCommentToDatastoreHandlerTest extends WebTestCase
{
  use FixturesTrait;

  /** @test */
  public function itHandlesAComment(): void
  {
    $this->loadFixtures([]);

    $container = self::bootKernel()->getContainer();

    $request = file_get_contents(__DIR__ . '/AddCommentToDatastoreHandlerTest/request.json');

    $serializer = $container->get('jms_serializer');

    $event = $serializer->deserialize($request, CommentUpsertedEvent::class, 'json');

    $repository = $container->get('Test.CommentRepository');
    $factory = $container->get('Test.CommentFactory');

    $handler = new AddCommentToDatastoreHandler($repository, $factory);
    $handler->handle($event);

    $this->assertEquals(1, count($repository->findAll()));
  }
}
