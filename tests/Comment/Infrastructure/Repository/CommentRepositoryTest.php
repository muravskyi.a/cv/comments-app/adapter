<?php

namespace App\Tests\Comment\Infrastructure\Repository;

use App\Comment\Domain\Entity\Comment;
use App\Tests\PrivatePropertyManipulatorTrait;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class CommentRepositoryTest extends KernelTestCase
{
  use PrivatePropertyManipulatorTrait;
  use FixturesTrait;

  private string $commentId = '19264c50-1fdb-4648-aa90-15f9bacf1928';
  private string $userID = 'abc78928-2419-4d0f-bce3-f75e74a727ca';
  private string $topicId = '8ddaae1f-70ba-4de7-b16b-b0f300222258';
  private string $commentText = 'repository test comment';

  /** @test */
  public function itSavesACommentToTheDatastore(): void
  {
    $this->loadFixtures([]);

    $comment = $this->getComment();
    $repository = self::$container->get('Test.CommentRepository');

    $this->assertEquals(0, count($repository->findAll()));

    $repository->save($comment);

    $this->assertEquals(1, count($repository->findAll()));

    $commentFromRepository = $repository->findOneBy(["comment" => $this->commentText]);

    $this->assertEquals($this->topicId, $this->getByReflection($commentFromRepository, 'topicId'));
    $this->assertEquals($this->userID, $this->getByReflection($commentFromRepository, 'userId'));
    $this->assertInstanceOf(\DateTime::class, $this->getByReflection($commentFromRepository, 'createdAt'));
    $this->assertInstanceOf(\DateTime::class, $this->getByReflection($commentFromRepository, 'updatedAt'));
  }

  private function getComment(): Comment
  {
    return new Comment($this->commentId, $this->userID, $this->topicId, $this->commentText);
  }
}
