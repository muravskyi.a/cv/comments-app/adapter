<?php

namespace App;

use Liip\FunctionalTestBundle\Test\WebTestCase;

final class IAmAliveControllerTest extends WebTestCase
{
  /** @test */
  public function IAmAliveReturnsStatusCode200(): void
  {
    $client = $this->createClient();
    $client->request(
      'GET',
      '/adapter/comments/i-am-alive'
    );

    $this->assertEquals(200, $client->getResponse()->getStatusCode());
  }
}
