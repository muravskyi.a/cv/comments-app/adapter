<?php

namespace App;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

final class IAmAliveController extends AbstractController
{
  public function handle(): JsonResponse
  {
    return new JsonResponse('i-am-alive');
  }
}
