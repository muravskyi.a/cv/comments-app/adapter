<?php

namespace App\Comment\Application;

use App\Comment\Domain\EntityFactory\CommentFactoryInterface;
use App\Comment\Domain\Repository\CommentRepositoryInterface;
use App\Comment\Domain\ValueObject\CommentUpsertedEvent;

final class AddCommentToDatastoreHandler
{
  private $repository;

  public function __construct(CommentRepositoryInterface $repository, CommentFactoryInterface $factory)
  {
    $this->repository = $repository;
    $this->factory = $factory;
  }

  public function handle(CommentUpsertedEvent $event)
  {
    $comment = $event->getData();

    $commentEntity = $this->factory->createFromValueObject($comment);

    $this->repository->save($commentEntity);
  }
}
