<?php

namespace App\Comment\Domain\ValueObject;

use App\Comment\Domain\ValueObject\Comment;

final class CommentUpsertedEvent
{
  private $data;

  public function __construct($data)
  {
    $this->data = $data;
  }

  public function getData(): Comment
  {
    return $this->data;
  }
}
