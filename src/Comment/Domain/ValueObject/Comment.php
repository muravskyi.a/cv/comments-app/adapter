<?php

namespace App\Comment\Domain\ValueObject;

final class Comment
{
  private string $commentId;
  private string $userId;
  private string $topicId;
  private string $comment;

  public function getCommentId(): string
  {
    return $this->commentId;
  }

  public function getUserId(): string
  {
    return $this->userId;
  }

  public function getTopicId(): string
  {
    return $this->topicId;
  }

  public function getComment(): string
  {
    return $this->comment;
  }
}
