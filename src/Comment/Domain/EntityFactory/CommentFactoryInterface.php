<?php

namespace App\Comment\Domain\EntityFactory;

use App\Comment\Domain\Entity\Comment;
use App\Comment\Domain\ValueObject\Comment as ValueObjectComment;

interface CommentFactoryInterface
{
  public function createFromValueObject(ValueObjectComment $comment): Comment;
}
