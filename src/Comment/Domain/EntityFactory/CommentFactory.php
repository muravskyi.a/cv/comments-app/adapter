<?php

namespace App\Comment\Domain\EntityFactory;

use App\Comment\Domain\Entity\Comment;
use App\Comment\Domain\ValueObject\Comment as ValueObjectComment;

final class CommentFactory implements CommentFactoryInterface
{
  public function createFromValueObject(ValueObjectComment $comment): Comment
  {
    $commentEntity = new Comment(
      $comment->getCommentId(),
      $comment->getUserId(),
      $comment->getTopicId(),
      $comment->getComment()
    );

    return $commentEntity;
  }
}
