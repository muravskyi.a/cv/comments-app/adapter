<?php

namespace App\Comment\Domain\Entity;

use DateTime;

final class Comment
{
  private int $id;
  private string $commentId;
  private string $userId;
  private string $topicId;
  private string $comment;
  private ?DateTime $createdAt = null;
  private ?DateTime $updatedAt = null;

  public function __construct(
    string $commentId,
    string $userId,
    string $topicId,
    string $comment
    )
  {
    $this->commentId = $commentId;
    $this->userId = $userId;
    $this->topicId = $topicId;
    $this->comment = $comment;
  }

  public function updateTimestamps(): void
  {
    $now = new \DateTime('now');

    $this->setUpdatedAt($now);

    if ($this->getCreatedAt() === null) {
        $this->setCreatedAt($now);
    }
  }

  private function getCreatedAt(): ?DateTime
  {
    return $this->createdAt;
  }

  private function setCreatedAt(DateTime $createdAt): void
  {
    $this->createdAt = $createdAt;
  }

  private function setUpdatedAt(DateTime $updatedAt): void
  {
    $this->updatedAt = $updatedAt;
  }
}
