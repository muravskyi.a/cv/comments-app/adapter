# Comments App Adapter

__Comments App Adapter__ is web service based on Symfony 5 framework as example of my using Symfony framework with implementing TDD & DDD methodologies and CQRS pattern. It's command service of CQRS pattern implementation.

## Environments

- DATABASE_URL: by default postgres://postgres:root@127.0.0.1:3307/comments

- AWS_KEY: AWS IAM service user access key
- AWS_SECRET: AWS IAM user sekret key
- AWS_REGION: Set in AWS region in which was created SQS queue and SNS topic
- AWS_QUEUE_ARN: Amazon Resource Name which is set in SQS service for created queue when click on it in SQS queues list
  Use link like `https://sqs.{region}.amazonaws.com/`

## Run app with docker-compose

```
docker-compose up
```

## Run queue consumer (in docker container)

```bash
bin/console bnnvara:simplebus:consume adapter-comments
```

## Testing

To test application execute command in your terminal
```bash
vendor/bin/phpunit
```

---

## Implementation steps

1. Install **Symfony 5** `composer create-project symfony/skeleton adapter`
1. Copy and past Docker files for running php, nginx and database in docker containers
1. Install PhpUnit bridge `composer require --dev symfony/phpunit-bridge`
1. Install Liip functional test bundle `composer require liip/functional-test-bundle`
1. Configure VSCode IDE
   * Add extensions for class generation
   * Add settings in .vscode/launch.json to connect xdebug with docker container
1.  Create `IAmAlive` health check functionality
    * Create test class `IAmAliveControllerTest` with testing method `handle`
     `tests/IAmAliveControllerTest.php`
    * Create src `IAmAliveController` controller with requests handler method `handle`
     `src/IAmAliveController.php`
    * Add route configuration in routes configuration
     `config/routes.yaml`
1. Create folder `tests/Comment` for storing `Comment` service tests.
    It's better to store service files in specific directory named by service name.
1. Create test DDD folder `tests/Comment/Application` with test for event handler `AddCommentToDatastoreHandlerTest` and `handle` method
    `tests/Comment/Application/AddCommentToDatastoreHandler.php`.
1. Create test folder with request/response data examples for using them in tests
    `tests/Application/AddCommentToDatastoreHandlerTest/{request.json, response.json}`
1. Create folder `src/Comment` for storing `Comment` service files.
1. Create src DDD folder `Application` with `AddCommentToDatastoreHandler` and `handle` method
    `src/Comment/Application/AddCommentToDatastoreHandler.php`.
1. Install `jms serializer bundle` to serialize/deserialize json into ValueObjects
     `composer require jms/serializer-bundle`.
1. Add value object `src/Comment/Domain/ValueObject/CommentUpsertedEvent.php` which is deserialized object from insert/update event got from the bus
2. Add value object `Comment` for proper deserialization `CommentUpsertedEvent` which store comment properties nested in event **data** property. We need exclude `Comment` value object from `CommentUpsertedEvent` by using JMS serializer configuration.
    `src/Comment/Domain/ValueObject/Comment.php`.
3. Add jms serializer configuration `config/packages/jms_serializer.yaml` where specify namespaces of available for serialization data objects `CommentUpsertEvent` and `Comment` and location of this objects serialization configuration.
4. Add serializer configurations `src/Comment/Application/config/serializer/valueObject/{Comment.xml, CommentUpsertedEvent}` with set deserialization `CommentUpsertedEvent` in `Comment` data object. Serializer configuration set in Application folder because serializer is part of business logic and not belongs only for Infrastructure layer.
5. Add `src/Comment/Domain/Entity/Comment.php` entity which will be created by `AddCommentToDatastoreHandler` after handling event.
6. Add repository test `tests/Comment/Infrastructure/Repository/CommentRepositoryTest.php` to test repository which will be used for searching comment in datastore after upsert event handling. Comment entity props will set by serialization. Till our code besides tests doesn't use getting props we will use custom trait which will get props by using php `ReflectionProperty` class and `tests/PrivatePropertyManipulatorTrait.php` trait. Also you should extends **KernelTestCase** instead **TestCase** because we need to use services from DI container which is not available from TestCase.
7. Add src repository `src/Comment/Infrastructure/Repositiory/CommentRepository.php`
8. Add src repository interface `src/Comment/Domain/Repository/CommentRepositoryInterface.php`
9. Install doctrine bundle `composer require doctrine/doctrine-bundle`
10. Install doctrine orm `composer require doctrine/orm`
11. Install doctrine migrations `composer require doctrine/doctrine-migrations-bundle`
12. Update Doctrine configuration in `config/packages/doctrine.yaml`. Set dbal configuration with db version and entities mapping configuration for orm.
13. Add orm configuration in Application directory `src/Comment/Infrastructure/config/orm/Comment.orm.xml`. It saved in Infrastructure layer because Doctrine used only for communication with DB storage which is the external component and doesn't belongs to your domain. Used xml instead annotations because annotations of entities creates hard coupling of Doctrine with your domain entities. Domain shouldn't be depended on Infrastructure or Application layer.
14. Add getting `'Test.App\Comment\Infrastructure\Repository\CommentRepository` for test from DI container. For it create test services configuration `config/services_test.yaml` where you can set public property as **true** for getting access for it in test. In test configuration use alias `@App\Comment\Infrastructure\Repository\CommentRepository` which will get service configuration which we will add in `config/services.yaml` config file.
15. Define `App\Comment\Infrastructure\Repository\CommentRepository` service for injection it in `AddCommentToDatastoreHandler` for saving comment.
16. Add injecting  `CommentRepository` in `AddCommentToDatastoreHandler` as input parameter of constructor method.
17. Add test for comment entities factory  `tests/Comment/Domain/EntityFactory/CommentFactoryTest.php` which will be created to separate responsibility of entity creation and event handling in `AddCommentToDatastoreHandler`
18. Add src comment entities factory `src/Comment/Domain/EntityFactory/CommentFactory.php` and set implementation of it's interface
19. Add interface `src/Comment/Domain/EntityFactory/CommentFactoryInterface.php` to use dependency on abstraction instead dependency on concrete implementation (D from SOLID)
20. Add injecting `CommentFactory` in `AddCommentToDatastoreHandler` as input parameter of constructor method.
21. Register `CommentFactory` in `config/services_test.yaml` with using alias on this service in `config/services.yaml`
22. Register `CommentFactory` in `config/services.yaml`
23. Add automatic injecting `CommentFactory` in `AddCommentToDatastoreHandler` in `config/services.yaml`
24. Create your databases for dev and test environment. I create comment and comment_test databases via using **HeidiSQL** utility. Connect to database via using localhost and default *root* login and *root* password which are set in docker_compose.
25. Configure your env files. For it at first get IP of your docker container with DB. For it find DB container in list of currently run. For getting list run command in console `docker ps`. There you can see `adapter_db_1` container if you use the same docker-compose file. Then run command `docker inspect adapter_php_1` and in the end of output find `Gateway` property of `Networks` block. IP set in `Gateway` is host for connecting to database. Set this IP in your test env file. In **VS Code** with installed extension **Docker** getting inspection output available by opening docker menu from left side panel and after right mouse button click on db container chose **Inspect** item from opened context menu. If you have installed php locally and want run migration not from container you should use your localhost 127.0.0.1 instead docker container gateway IP.
26. Run generating migration. For it open console where PHP is available. In our case we use docker container so we should open bash in container and execute command `bin/console doctrine:migrations:diff`. In **VS Code** is available extension **Docker** which alow easy open bash in container. After installation you will find Docker icon in left side panel. I use wsl and Docker is running from windows shell because docker-compose has issues when `docker-compose up` command is executed from wsl. So for opening bash in container I will check name of container via executing command `docker ps` and use command `docker exec -it adapter_php_1 bash` for accessing bash in container.
27. Install ORM fixtures `composer require --dev orm-fixtures`. It need to clean up database before each test and add records for testing if we need them for tests.
28. Install Liip test fixtures bundle `composer require liip/test-fixtures-bundle` which will run fixture loading by calling method from test class which use Liip fixtures trait instead need to run console command available in `orm-fixtures`.
29. Add using fixture in `CommentRepositoryTest`.
30. Run test
31. Add subscribing on event bus
  I use **AWS SQS** as service bus and library `bnnvara/simple-bus-aws-bridge-bundle` to subscribe on messages. So lets install this library and configure AWS SQS/SNS. Skip this steps if you use other solutions like RabbitMQ, Kafka etc.
  For installation it execute command and execute receipt when it will be asking you about that.
  ```bash
  composer require bnnvara/simple-bus-aws-bridge-bundle
  ```
  I got exception when tried to install it in current version on stage `bin/console clean:cache`.
  As work around we need at first add configuration which described bellow, connect bundles and repeat installation.
  - Add `config/packages/simple_bus_aws_bridge.yaml` configuration file
  - Add bundles in `config/bundles.php`.
    ```
    ...
    SimpleBus\AsynchronousBundle\SimpleBusAsynchronousBundle::class => ['all' => true],
    JMS\SerializerBundle\JMSSerializerBundle::class => ['all' => true],
    SimpleBus\JMSSerializerBundleBridge\SimpleBusJMSSerializerBundleBridgeBundle::class => ['all' => true],
    BNNVARA\SimpleBusAwsBridgeBundle\SimpleBusAwsBridgeBundle::class => ['all' => true],
    SimpleBus\SymfonyBridge\SimpleBusCommandBusBundle::class => ['all' => true],
    SimpleBus\SymfonyBridge\SimpleBusEventBusBundle::class => ['all' => true],
    ...
    ```
  - Run composer installation if it was failed before
  ```bash
  composer require bnnvara/simple-bus-aws-bridge-bundle
  ```
  - If you didn't create SNS topic and SQS queue
    * Create topic in AWS SNS service
    * Create queue in AWS SQS service
    * Make queue subscription on topic
    * Create user in AWS IAM service
  - Copy AWS credentials in your .env.local file (see AWS steps below)
    * Copy **Access key ID** in .env.local `AWS_KEY`
    * Copy **Secret access key** in .env.local `AWS_SECRET`
    * Copy region set in AWS top right corner in .env.local `AWS_REGION`
    * Copy topic from SNS in .env.local `AWS_TOPIC_ARN`
    * Set **2010-03-31** in .env.local `AWS_SDK_VERSION`
  - For subscribing `AddCommentToDatastoreHandler` on `CommentUpsertedEvent` add in `config/services.yaml` tag for `AddCommentToDatastoreHandler` service
    ```
    tags:
      - { name: 'event_subscriber', subscribes_to: 'App\Comment\Domain\ValueObject\CommentUpsertedEvent' }
    ```
    It's important to have class which is set in serialized event message from publisher service equal to class in subscribed services. Otherwise library can't deserialize event. We already create value object for `CommentUpsertedEvent` with the same class as in API service which responsible for publishing this messages. For insurance of full classes matching is recommended to create composer vendor package for events. After installing this package you will always have the same namespace in your services.
  - Run events consumer from php container terminal
    * Open php container terminal
      ```bash
      docker exec -it adapter_php_1 bash
      ```
    * Execute command to subscribe on created in SNS topic `adapter-comment`
      ```bash
      bin/console bnnvara:simplebus:consume adapter-comment
      ```

### Setup AWS services

* Login into aws
* Check region which you use in right top corner of top menu. Use location closer to your users
* Open services lists

#### SNS - Simple Notification Service

* In search form enter `sns` and open it
* Click on **Create topic** button
* Enter **Name** and **Display name** of your topic. In our case we use `CommentUpsertedEvent`.
  Recommended to set suffix of environment which will use this topic (-dev, -prod etc)
* Click on **Create topic** button

#### SQS - Simple Queue Service

* In search form enter `sqs` and open it
* Click on **Get Started Now** button
* Type Queue name.
* By default is selected standard queue.
* Click on **Quick-Create Queue** button.
* In list of created queues make mouse right button click on newly created queue and click on `Subscribe Queue to SNS Topic` from displayed context menu
* In opened form choose your region which you use when you create queue. By default it should be already set correct. Choose topic from dropdown list which will display after click on **Choose a Topic** input field.
* Click **Subscribe** button

#### IAM - Add User for getting credentials

* In search form enter `iam` and click on found item to open this service panel
* In opened page click on `Users` in left panel
* Click on **Add user** button
* Enter user name. You can use something like `ci-comments-app`. I use `ci-` prefix in names to identify that it is console interface user.
* Enable **Programmatic access** checkbox and click on button `Next: Permissions`
* In new opened page click on **Create Group**
* Type name for new group. To detect permissions enabled for this group you can set name like "SQS-SNS" because we will enable access on SQS and SNS services for this group.
* In input field bellow type SNS and choose **AmazonSNSFullAccess**. Than do the same for service SQS.
* Click on **Next: Tags** button
* In this form you can set tags which latter can be used in different proposes like tracking billing. For now I just skip it and click button **Next: Review**
* On new opened page click button **Create user**
* Now you can see your new users credentials.
